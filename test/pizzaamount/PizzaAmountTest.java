package pizzaamount;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import static org.hamcrest.CoreMatchers.*;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author
 * KEN
 */
public class PizzaAmountTest {

	public PizzaAmountTest() {
	}

	@BeforeClass
	public static void setUpClass() {
	}

	@AfterClass
	public static void tearDownClass() {
	}

	@Before
	public void setUp() {
	}

	@After
	public void tearDown() {
	}
	// TODO add test methods here.
	// The methods must be annotated with annotation @Test. For example:
	//
	// @Test
	// public void hello() {}

	@Test
	public void Pizza_set_getName() throws Exception {
		Pizza pizza = new Pizza(MenuSize.L, PizzaMenuList.マルゲリータ);
		assertThat(pizza.getName(), is(PizzaMenuList.マルゲリータ));
	}

	@Test
	public void Pizza_set_getSize() throws Exception {
		Pizza pizza = new Pizza(MenuSize.L, PizzaMenuList.マルゲリータ);
		assertThat(pizza.getSize(), is(MenuSize.L));
	}

	@Test
	public void ピザ_マルゲリータLを注文_1000_enum() throws Exception {
		Pizza pizza = new Pizza(MenuSize.L, PizzaMenuList.マルゲリータ);
		PizzaCalc sut = new PizzaCalc();
		sut.add(pizza);
		assertThat(sut.getAmount(), is(1000));
	}

	@Test
	public void ピザ_マルゲリータL_サラミLを注文_1600() throws Exception {
		Pizza pizza = new Pizza(MenuSize.L, PizzaMenuList.マルゲリータ);
		Pizza pizza2 = new Pizza(MenuSize.L, PizzaMenuList.サラミ);
		PizzaCalc sut = new PizzaCalc();
		sut.add(pizza);
		sut.add(pizza2);

		assertThat(sut.getAmount(), is(1600));
	}

	@Test
	public void ピザ_ハーフ_マルゲリータ_サラミ_800() throws Exception {

		HalfPizza sut = new HalfPizza(new Pizza(MenuSize.L, PizzaMenuList.マルゲリータ), new Pizza(MenuSize.L, PizzaMenuList.サラミ));
		assertThat(sut.getAmount(), is(800));

	}

	@Test
	public void ナゲットを注文_500() throws Exception {
		Sidemenu sidemenu = new Sidemenu("ナゲット", 500);
		PizzaCalc sut = new PizzaCalc();
		sut.add(sidemenu);
		assertThat(sut.getAmount(), is(500));
	}

	@Test
	public void ドリンクを注文_300() throws Exception {
		Drink drink = new Drink("L", "コーラ", 300);
		PizzaCalc sut = new PizzaCalc();
		sut.add(drink);
		assertThat(sut.getAmount(), is(300));
	}
}