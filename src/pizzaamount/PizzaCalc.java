/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pizzaamount;

import java.util.ArrayList;

/**
 *
 * @author
 * KEN
 */
public class PizzaCalc {

	private ArrayList<Pizza> order;
	private ArrayList<Sidemenu> sidemenuOder;
	private ArrayList<Drink> drinkOder;

	public PizzaCalc() {
		this.order = new ArrayList<>();
		this.sidemenuOder = new ArrayList<>();
		this.drinkOder = new ArrayList<>();

	}

	public int getAmount() {
		int totalAmount = 0;
		for (Pizza pizza : order) {
			totalAmount = totalAmount + pizza.getAmount();
		}


		for (Sidemenu sidemenu : sidemenuOder) {
			totalAmount = totalAmount + sidemenu.getAmount();
		}
		for (Drink drink : drinkOder) {
			totalAmount = totalAmount + drink.getAmount();
		}

		return totalAmount;
	}

	void add(Pizza pizza) {
		order.add(pizza);
	}

	void add(Sidemenu sidemenu) {
		sidemenuOder.add(sidemenu);
	}

	void add(Drink drink) {
		drinkOder.add(drink);
	}
}
