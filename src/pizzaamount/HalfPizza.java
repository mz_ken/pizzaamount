/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pizzaamount;

/**
 *
 * @author
 * KEN
 */
class HalfPizza {
	
	private Pizza pizza1;
	private Pizza pizza2;
	
	public HalfPizza(Pizza pizza1, Pizza pizza2) {
		this.pizza1 = pizza1;
		this.pizza2 = pizza2;
	}
	
	public int getAmount(){
		return (this.pizza1.getAmount() + this.pizza2.getAmount())/2;
	}
}
