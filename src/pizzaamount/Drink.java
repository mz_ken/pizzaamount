/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pizzaamount;

/**
 *
 * @author
 * KEN
 */
public class Drink {
		private String name;
	private int amount;
	private String size;

	public Drink(String size, String name, int amount) {
		this.name = name;
		this.amount = amount;
		this.size = size;
	}

	public String getName(){
		return this.name;
	}
	public String getSize(){
		return this.size;
	}
	public int getAmount(){
		return this.amount;
	}
	
}
