package pizzaamount;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author
 * KEN
 */
public class Pizza {

	private int amount;

	private PizzaMenuList pizzaname;
	private MenuSize menuSize;


	Pizza(MenuSize menuSize, PizzaMenuList pizzaMenuList) {
		
		this.pizzaname = pizzaMenuList;
		this.menuSize = menuSize; 
		
		if(pizzaMenuList == PizzaMenuList.マルゲリータ){
			this.amount = 1000;
		}else{
			this.amount = 600;
		}
	}

	public PizzaMenuList getName(){
		return this.pizzaname;
	}
	public MenuSize getSize(){
		return this.menuSize;
	}
	public int getAmount(){
		return this.amount;
	}
	
}
